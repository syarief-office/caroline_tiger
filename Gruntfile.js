/*

TO DO

1) Reduce CSS duplication
   - Ideally just a single build - global.scss turns into /build/global.css
   - Can Autoprefixer output minified?
   - If it can, is it as good as cssmin?
   - Could Sass be used again to minify instead?
   - If it can, is it as good as cssmin?

2) Better JS dependency management
   - Require js?
   - Can it be like the Asset Pipeline where you just do //= require "whatever.js"

3) Is HTML minification worth it?

4) Set up a Jasmine test just to try it.

5) Can this Gruntfile.js be abstracted into smaller parts?
   - https://github.com/cowboy/wesbos/commit/5a2980a7818957cbaeedcd7552af9ce54e05e3fb

*/


module.exports = function(grunt) {

  // Utility to load the different option files
  // based on their names
  function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*', {cwd: path}).forEach(function(option) {
      key = option.replace(/\.js$/,'');
      object[key] = require(path + option);
    });

    return object;
  }

  // Initial config
  var config = {
    pkg: grunt.file.readJSON('package.json')
  }

   grunt.initConfig({
      concat: {
        basic_and_extras: {
          files: {
            'dist/assets/js/libs/libs.min.js': 'app/js/libs/{,*/}*.js',
            'dist/assets/js/modernizer/modernizr.min.js': 'app/js/modernizer/modernizr.custom.js',
            //'dist/assets/js/vendor/vendor.js': ['app/js/vendor/jquery.slicknav.js', ],
            'dist/assets/js/global.js': ['app/js/global.js']
          }
        }
      },

      uglify: {
        my_target: {
          files: {
            'dist/assets/js/libs/libs.min.js': ['app/js/libs/{,*/}*.js'],
            'dist/assets/js/modernizer/modernizr.min.js': 'app/js/modernizer/modernizr.custom.js'
          }
        }
      },

      compass: {
        options: {
            sassDir: 'app/sass',
            cssDir: 'dist/assets/css',
            imagesDir: 'app/img',
            javascriptsDir: 'app/js',
            fontsDir: 'app/fonts',
            relativeAssets: true
        },
        dist: {},
        server: {
            options: {
                debugInfo: false
            }
        }
      },

      //autoprefixer: {
        //dist: {
            //files: {
            //    'dist/assets/css/main.css': 'app/sass/{,*/}*.{sass, scss}'
            //}
        //} 
      //},

      imagemin: {
        dynamic:{     
            files: [{
              expand: true,                  // Enable dynamic expansion
              cwd: 'app/',                   // Src matches are relative to this path
              src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
              dest: 'dist/assets'                  // Destination path prefix
            }]
          }
      },

      assemble: {
          options: {
              flatten: true,
              layout: 'app/template/layouts/default.hbs',
              partials: ['app/template/partials/*.hbs'],
          },
          pages: {
              files: {
                  'dist/': ['app/template/pages/*.hbs', '!app/templates/pages/index.hbs']
              }
          },
          index: {
              files: {
                  'dist/': ['app/template/pages/index.hbs']
              }
          },
      },

      htmlmin: {
            dist: {
                options: {
                    /*removeCommentsFromCDATA: true,
                    // https://github.com/yeoman/grunt-usemin/issues/44
                    //collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeAttributeQuotes: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true*/
                },
                files: [{
                    expand: true,
                    cwd: 'dist/',
                    src: '*.html',
                    dest: 'dist/'
                }]
            }
        },

      connect: {
        server: {
          options: {
            port: 9001,
            hostname: 'localhost'
          }
        }
      },

      watch: {
        options: {
          livereload: true,
        },
         compass: {
              files: ['app/sass/{,*/}*.{scss,sass}'],
              tasks: ['compass:server']
          },
          uglify: {
              files: ['app/js/libs/{,*/}*.js'],
              tasks: ['uglify']
          },
          concat: {
            files: ['app/js/{,*/}*.js'],
            tasks: ['concat:basic_and_extras']
          },
          //*autoprefixer: {
            //files: ['app/sass/partials/{,*/}*.{sass, scss}'],
            //tasks: ['autoprefixer:dist']
          //},
          imagemin: {
            files: ['{,*/}*.{png,jpg,jpeg}'],
            tasks: ['imagemin:dynamic']
          },
          assemble: {
            files: ['app/template/{,*/}*.hbs'],
            tasks: ['assemble']
          }

          //livereload: {
          //    files: [
           //       'app/*.html',
           //       'app/templates/{,*/}*.hbs',
           //       'app/{,*/}*.{css,scss,sass}',
            //      'app/{,*/}*.js',
            //      'app/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
            //  ],
            //  tasks: ['assemble', 'livereload']
          //}
      }

   });



  // Load tasks from the tasks folder
  //grunt.loadTasks('tasks');
  // Load all the tasks options in tasks/options base on the name:
  // watch.js => watch{}
  //grunt.util._.extend(config, loadConfig('./tasks/options/watch{}'));
  
  //grunt.initConfig(config);
  //require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  //grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default Task is basically a rebuild
  grunt.registerTask('default', ['concat', 'uglify','compass', 'imagemin','assemble', 'htmlmin', 'connect', 'watch']);
  // Moved to the tasks folder:
  //grunt.registerTask('dev', ['connect', 'watch']);

};
